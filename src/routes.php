<?php

// Routes

////////////////////////////// Example //////////////////////////////
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

/////////////////////////////////////////////////////////////////////



// Section containing all routes for the API

$app->group('/api',function() use ($app) { // First part of the path (all url start by /api)

    $app->group('/v1',function() use ($app) { // Second part of the path (all url start by /api/v1)

		////////////////////////// Book Section ////////////////////////////////////////////////
	
	// "/api/v1/books"   	Get all books
        $app->get('/books',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->getBooks($request));
            $newResponse = $response->withHeader(
                'Content-type',
                'application/json; charset=utf-8'
			);

            return $newResponse;
        });
	
	// "/api/v1/books/{id}" 	Get a book by its id    
        $app->get('/books/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->getBook($request));
            $newResponse = $response->withHeader(
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	// "/api/v1/search/{keyword}" Search books by keyword (They keyword can be about title,author,category)
        $app->get('/books/search/{keyword}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->searchBook($request));
            $newResponse = $response->withHeader(      // Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });

	// "/api/v1/books"	Add a book to the media library    
        $app->post('/books',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->addBook($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/books/{id}"		Edit a specific book    
	$app->put('/books/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->editBook($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/books/{id}"		Delete a specific book    
        $app->delete('/books/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(bookManager::Instance()->deleteBook($request));
            
            return $response;
        });
		
		///////////////////////////////////////////////////////////////////////////////
		
		//////////////////////// Entity Section ///////////////////////////////////////
	
	// "/books{id_book}/entity"	Add an entity for a specific book    
	$app->post('/books/{id_book}/entity',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(entityManager::Instance()->addEntity($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/entity/{id_entity}"	Edit a specific entity    
	$app->put('/entity/{id_entity}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(entityManager::Instance()->editEntity($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/entity/{id_entity}"	Delete a specific entity    
        $app->delete('/entity/{id_entity}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(entityManager::Instance()->deleteEntity($request));
            
            return $response;
        });
		
		//////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////// Member Section ///////////////////////////////
        
	// "/api/v1/members"	Get all members    
	$app->get('/members',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::Instance()->getMembers($request));
            $newResponse = $response->withHeader(
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/members{id}"	Get a specific member    
	$app->get('/members/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::Instance()->getMember($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
		
	// "/api/v1/members"    Add a member
	$app->post('/members',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::Instance()->addMember($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/members/{id}"	Edit a specific member    
	$app->put('/members/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::Instance()->editMember($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/members/{id}"	Delete a specific member    
	$app->delete('/members/{id}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::Instance()->deleteMember($request));
            
            return $response;
        });

	// "/api/v1/members/{id}/books"		Get all books borrowed by a member (list of loans) 
        $app->get('/members/{id}/books',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(memberManager::getMemberLoans($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
		
		
		////////////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////// Loan Section //////////////////////////////////////
	
	// "/api/v1/loan"	Add a new loan    
	$app->post('/loan',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(loanManager::addLoan($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/loan/{id_loan}"    Edit a specific loan
	$app->put('/loan/{id_loan}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(loanManager::editLoan($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
	
	// "/api/v1/loan{id_loan}"	Delete a specific loan    
	$app->delete('/loan/{id_loan}',function(\Slim\Http\Request $request,\Slim\Http\Response $response) {
            $response->getBody()->write(loanManager::deleteLoan($request));
            $newResponse = $response->withHeader(	// Response in json and utf-8
                'Content-type',
                'application/json; charset=utf-8'
            );

            return $newResponse;
        });
		
		//////////////////////////////////////////////////////////////////////////////////////////

    });

});


