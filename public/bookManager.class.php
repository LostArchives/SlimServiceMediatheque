<?php
/**
Singleton Class to handle all queries related to the book table
**/

class bookManager {
	
    private static $instance = null;

    private function __construct()
    {
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new bookManager();
        }

        return self::$instance;
    }

    /*
    Method to get all books
    */
    public function getBooks($response) {

        $sql = "SELECT * FROM book";

        try {
            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $cnt =0;
            foreach ($wines as $wine) {		// For utf-8 encoding (if not present, json_encode fails or character problems)
                $wines[$cnt]['name'] = utf8_encode($wine['name']);
                $wines[$cnt]['category'] = utf8_encode(($wine['category']));
                $wines[$cnt]['author'] = utf8_encode(($wine['author']));
                $cnt++;
            }
            return json_encode($wines);	// Return in json the utf-8 encoded data
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    /*
    Get a specific book
    */
    public function getBook($request) {

        $id = $request->getAttribute('id'); // Id of the book

        $sql = "SELECT book.* FROM book WHERE book.id=".$id;	// Get book data

        $sql_entities = "SELECT book_entity.* FROM book_entity WHERE book_entity.id_book=".$id; //Get books entites data

        try {

            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_ASSOC); // book data

            $stmt_ent = getConnection()->query($sql_entities);
            $wines_ent = $stmt_ent->fetchAll(PDO::FETCH_ASSOC); //book entites for the book

                $book_entities = array(); // prepare the array of book_entites
                foreach($wines_ent as $wine) {
                    $book_entities[] = $wine; //Set the book_entites to the array
                }

                $book = $wines[0]; //Get the book data (one result possible only because of the id)
		
                foreach($book as $key=>$value) { // Utf-8 encoding
                    $book[$key] = utf8_encode($book[$key]);
                }
		
		// The last key of the returned array (book_entities) is the array of book_entities
                $book['book_entities'] = $book_entities; 

            return json_encode($book);
		
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }


    }

    /*
    Add a book to the media library
    */
    public function addBook($request) {

        $book = json_decode($request->getBody());

        $sql = "INSERT INTO book VALUES ('',:name,:image_url,:category,:author,0,0)"; //Query with parameters

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("name",$book->name); // Bind the query parameter ':name'
            $stmt->bindParam("category",$book->category);// Bind the query parameter ':category'
	    $stmt->bindParam("author",$book->author);// Bind the query parameter ':author'
	    $stmt->bindParam("image_url",$book->image_url);// Bind the query parameter ':image_url'
            $stmt->execute();
            $book->id = $db->lastInsertId();
            $db = null;
            echo json_encode($book);


        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }

    /*
    Edit a specific book
    */
    public function editBook(\Slim\Http\Request $request) {

        $book = json_decode($request->getBody());

        $id = $request->getAttribute('id'); // Id of the book to edit
	    
        $sql = "UPDATE book SET name=:name,category=:category,author=:author,image_url=:image_url WHERE id=:id"; // Query with parameters

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("name",$book->name); // Bind the query parameter ':name'
            $stmt->bindParam("category",$book->category); // Bind the query parameter ':category'
            $stmt->bindParam("author",$book->author); // Bind the query parameter ':author'
	    $stmt->bindParam("image_url",$book->image_url); // Bind the query parameter ':image_url'
            $stmt->bindParam("id",$id); // Bind the query parameter ':id'
            $stmt->execute();
            $book->id = $id;
            $db = null;
            echo json_encode($book);

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    /*
    Delete a specific book
    */
    public function deleteBook($request) {

        $id = $request->getAttribute('id');
        $sql = "DELETE FROM book WHERE id=:id";

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id',$id);
            $stmt->execute();
            $db = null;
            echo '1'; // If success , '1'

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }
	
    /*
    Search a book by keyword
    */
    public function searchBook($request) {
		
	$keyword = $request->getAttribute('keyword');
	// The keyword can refer to the title , category or author
	$sql = "SELECT * FROM book WHERE name LIKE '%".$keyword."%' OR category LIKE '%".$keyword."%' OR author like '%".$keyword."%'";
		
	try {
	    $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $cnt =0;
            foreach ($wines as $wine) { 		// Utf-8 encoding
                $wines[$cnt]['name'] = utf8_encode($wine['name']);
                $wines[$cnt]['category'] = utf8_encode(($wine['category']));
                $wines[$cnt]['author'] = utf8_encode(($wine['author']));
                $cnt++;
            }
            return json_encode($wines);
			
		} catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
	}


}


?>
