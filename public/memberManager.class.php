<?php

/**
Singleton class to handle all queries related to the memberTable
**/

class memberManager {

    private static $instance = null;

    private function __construct()
    {
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new memberManager();
        }

        return self::$instance;
    }

    /*
    Get all members
    */
    public function getMembers($response) {
		
		$sql = "SELECT * FROM member";

        try {
            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
			
			
            return json_encode($wines);
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
	}

    /*
    Get a specific member by its id
    */
    public function getMember($request) {
		
	$id = $request->getAttribute('id'); // Id of the member searched
		
	$sql = "SELECT * FROM member where id=".$id;

        try {
            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
			
            return json_encode($wines);
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
	}
	
    /*
    Add a member
    */
    public function addMember($request) {
		
	$member = json_decode($request->getBody()); // Member data to add

        $sql = "INSERT INTO member VALUES ('',:pseudo,:name,:avatar,:signup_date,0,0,0)";
		
	$date = date('Y-m-d'); // date of today for the signup date

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("pseudo",$member->pseudo);
            $stmt->bindParam("name",$member->name);
			$stmt->bindParam("avatar",$member->avatar);
			$stmt->bindParam("signup_date",$date);
            $stmt->execute();
            $member->id = $db->lastInsertId(); // Will be used later in the client to check if query was successful
            $db = null;
            echo json_encode($member);


        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
	}
	
      /*
      Edit a specific member
      */
      public function editMember($request) {
		
	$member = json_decode($request->getBody()); // Updated member data

	$id = $request->getAttribute('id');	// Id of the member to edit
		
        $sql = "UPDATE member SET name=:name,avatar=:avatar where id=".$id;

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("name",$member->name);
	    $stmt->bindParam("avatar",$member->avatar);
            $stmt->execute();
            $member->id = $id; // Will be used later in the client to check if query was successful
            $db = null;
            echo json_encode($member,true);


        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
	}
	
	/*
	Delete a specific member
	*/
	public function deleteMember($request) {
		
	$id = $request->getAttribute('id'); // Id of the member to delete
        $sql = "DELETE FROM member where id=:id";

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id',$id);
            $stmt->execute();
            $db = null;
            echo '1'; // If success , '1'

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
	}
	
	/*
	Get all loans of a specific member
	*/
	public function getMemberLoans($request) {

        $id_member = $request->getAttribute('id'); // Id of the member concerned by loans
		
        $sql = "select book.*,borrowed.* from book,book_entity,borrowed,member where member.id = borrowed.id_member and book_entity.id=borrowed.id_book_entity and book.id = book_entity.id_book and member.id=".$id_member;

        try {

            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $loans = array();
            foreach ($wines as $wine) {
                $book = array(
                    'id' => $wine['id'],
                    'name' => utf8_encode($wine['name']),
                    'image_url' => utf8_encode($wine['image_url']),
                    'category' => utf8_encode($wine['category']),
                    'author' => utf8_encode($wine['author']),
                    'stock_total' => $wine['stock_total'],
                    'stock_dispo' => $wine['stock_dispo']
                );
                $loan = array(
                    'id_loan' => $wine['id_loan'],
                    'id_book_entity' => $wine['id_book_entity'],
                    'start_date' => $wine['start_date'],
                    'end_date' => $wine['end_date'],
                    'finished' => $wine['finished'],
                    'book' => $book	// The book concerned by the loan (array)
                );
                $loans[] = $loan; // Add the loan object to loans (array of loan)
            }

            return json_encode($loans);
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }

	
}




?>
